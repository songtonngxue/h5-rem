import { createApp } from 'vue'
import "./styles/index.scss"
import 'amfe-flexible'
import App from './App.vue'



createApp(App).mount('#app')
